import paramiko
import time


def getRemoteFile(ip:str, user:str, pwd:str, remote_file_path:str, local_file_path:str, port:int=22) -> bool:
    """ get remote file """

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname=ip, port=port, username=user, password=pwd)

    sftp = ssh.open_sftp()
    sftp.get(remote_file_path, local_file_path)

    sftp.close()
    ssh.close()

    return True


def putRemoteFile(ip:str, user:str, pwd:str, remote_file_path:str, local_file_path:str, port:int=22) -> bool:
    """ put db file """

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname=ip, port=port, username=user, password=pwd)

    sftp = ssh.open_sftp()
    sftp.put(local_file_path, remote_file_path)

    sftp.close()
    ssh.close()

    return True


def callRemoteShell(ip:str, user:str, pwd:str, sh_file_path:str, port:int=22) -> None:
    """ put db file and reload server """
    time.sleep(0.5)

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname=ip, port=port, username=user, password=pwd)

    if sh_file_path:
        stdin, stdout, stderr = ssh.exec_command('sh '+sh_file_path)
        ret = stdout.read().decode()

    ssh.close()

