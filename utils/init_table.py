# -*- coding:utf-8 -*-
import os
import sys
import django

base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(base_dir)
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ioc_record.settings')
django.setup()


def init_ip_ioc_etc():
    from app_iocs import models
    if models.Iocs.objects.count() == 0:

        ip_obj = models.IPs(ip='10.1.192.78')
        ip_obj.save()

        ioc_obj = models.Iocs(
            name = 's7nodaveTest',
            file_path = '/home/dkqi/data/ioc_dbs',
            ip = ip_obj,
            port = 5014,
        )
        ioc_obj.save()

        for t in ['ai','ao','bi','bo','longin','longout','stringin','stringout']:
            models.PVTypes.objects.create(name=t)

        for t in ['SCAN','DTYP','OUT',]:
            models.PVFieldTypes.objects.create(name=t)

        pv_obj = models.PVs(
            name = 's7nodaveTest01',
            ioc = ioc_obj,
            ptype = models.PVTypes.objects.filter(name='longout').first()
        )
        pv_obj.save()

    print('finish')


if __name__ == '__main__':
    init_ip_ioc_etc()
