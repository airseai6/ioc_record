from django.apps import AppConfig


class AppIocsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app_iocs'
