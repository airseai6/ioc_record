from django.urls import path, re_path
from . import views
app_name = 'app_iocs'

urlpatterns = [
    re_path(r'', views.iocs, name='iocs'),
    re_path(r'^iocs.html$', views.iocs, name='iocs'),
]
