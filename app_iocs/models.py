from django.db import models


class IPs(models.Model):
    ip = models.CharField(verbose_name='IP地址', max_length=64, db_index=True,)
    username = models.CharField(verbose_name='远程主机用户', max_length=64,null=True, blank=True,)
    password = models.CharField(verbose_name='远程主机密码', max_length=64,null=True, blank=True,)
    remark = models.CharField(verbose_name='备注', max_length=512,null=True, blank=True,)
    index = models.SmallIntegerField(verbose_name='排序', default=1,)
    is_active = models.BooleanField(verbose_name='是否启用', default=True,)
    objects = models.Manager()
    def __str__(self):
       return f'{self.id} : {self.ip}'
    class Meta:
        ordering = ['index']
        verbose_name = 'ip'
        verbose_name_plural = 'ips'


class Iocs(models.Model):
    name = models.CharField(verbose_name='ioc名称', max_length=512, db_index=True)
    ip = models.ForeignKey(to=IPs, on_delete=models.DO_NOTHING, verbose_name='IP', null=True,)

    is_remote = models.BooleanField(verbose_name='是否远程', default=True,)
    file_path = models.CharField(verbose_name='db文件地址', max_length=1024,null=True, blank=True,)
    dbd_path = models.CharField(verbose_name='dbd文件地址', max_length=1024,null=True, blank=True,)
    sh_path = models.CharField(verbose_name='sh文件地址', max_length=1024,null=True, blank=True,)

    port = models.IntegerField(verbose_name='端口号',null=True, blank=True,)
    server_name = models.CharField(verbose_name='服务名称', max_length=1024,null=True, blank=True,)
    remark = models.CharField(verbose_name='备注', max_length=512,null=True, blank=True,)
    index = models.SmallIntegerField(verbose_name='排序', default=1,)
    is_protect = models.BooleanField(verbose_name='是否保护', default=False,)
    is_active = models.BooleanField(verbose_name='是否启用', default=True,)

    objects = models.Manager()
    def __str__(self):
       return f'name : {self.name}'
    class Meta:
        ordering = ['index',]
        verbose_name = 'Ioc'
        verbose_name_plural = 'Iocs'


class PVTypes(models.Model):
    name = models.CharField(verbose_name='pv 类型', max_length=512, db_index=True)

    objects = models.Manager()
    def __str__(self):
       return f'name : {self.name}'
    class Meta:
        verbose_name = 'PVType'
        verbose_name_plural = 'PVTypes'


class PVFieldTypes(models.Model):
    name = models.CharField(verbose_name='PV Fields 类型', max_length=512, db_index=True)

    objects = models.Manager()
    def __str__(self):
       return f'name : {self.name}'
    class Meta:
        verbose_name = 'PVFieldType'
        verbose_name_plural = 'PVFieldType'


class PVs(models.Model):
    name = models.CharField(verbose_name='pv名称', max_length=512, db_index=True)
    ioc = models.ForeignKey(to=Iocs, on_delete=models.DO_NOTHING, verbose_name='ioc', null=True,)
    ptype = models.ForeignKey(to=PVTypes, on_delete=models.DO_NOTHING, verbose_name='pv type', null=True,)
    remark = models.CharField(verbose_name='备注', max_length=512,null=True, blank=True,)
    is_active = models.BooleanField(verbose_name='是否启用', default=True,)

    objects = models.Manager()
    def __str__(self):
       return f'name : {self.name}'
    class Meta:
        verbose_name = 'PV'
        verbose_name_plural = 'PVs'


